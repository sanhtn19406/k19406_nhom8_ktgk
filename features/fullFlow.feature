Feature: Business rules for login
  In order to achieve my goals
  As a persona
  I want to be able to login step

  Scenario: Check login flow
    Given I login to Casso page
    And I go to create new company page
    And I create a new company
    And I create a API key
    And I edit API key to "123"
    And I delete API key
