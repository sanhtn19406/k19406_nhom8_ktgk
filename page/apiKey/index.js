const { I } = inject()
const timeout = require('../common/timeout')
const apiKeyLocator = require('./locator')
const customMethod = require("../../utils/customMethod")
const settingPageLocator = require('../settingPage/locator')

module.exports = {
    createApiKey(apiName) {
        customMethod.clickElement(settingPageLocator.menuIcon)
        customMethod.clickElement(settingPageLocator.settingTab)
        customMethod.clickElement(apiKeyLocator.apiTitle)
        I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)
        customMethod.clickElement(apiKeyLocator.createApiButton)
        customMethod.fieldValue(apiKeyLocator.nameApiField, apiName)
        customMethod.clickElement(apiKeyLocator.updateApiButton)
        I.waitForElement(apiKeyLocator.viewApiDiv, timeout.waitForElement)
        customMethod.clickElement(apiKeyLocator.xongApiButton)
        I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)

    },
    editApiKey(name) {
        // customMethod.clickElement(settingPageLocator.menuIcon)
        // customMethod.clickElement(settingPageLocator.settingTab)
        // customMethod.clickElement(apiKeyLocator.apiTitle)
        // I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)
        customMethod.clickElement(apiKeyLocator.editApiButton)
        customMethod.fieldValue(apiKeyLocator.nameApiField, name)
        customMethod.clickElement(apiKeyLocator.updateApiButton)
        I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)

    },
    deleteApiKey() {
        // customMethod.clickElement(settingPageLocator.menuIcon)
        // customMethod.clickElement(settingPageLocator.settingTab)
        // customMethod.clickElement(apiKeyLocator.apiTitle)
        // I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)
        customMethod.clickElement(apiKeyLocator.deleteApiButton)
        customMethod.clickElement(apiKeyLocator.confirmDeleteApiButton)
        I.waitForElement(apiKeyLocator.apiListTitle, timeout.waitForElement)

    }


}