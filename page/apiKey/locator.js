module.exports = {
    apiTitle: "//span[(text()='Api keys')]",
    createApiButton: "//button//span[text()='Tạo API key']",
    nameApiField: "//input[@formcontrolname='name']",
    apiListTitle: "//h4[contains(text(),'API Keys')]",
    xongApiButton: "//button//span[contains(text(),'Xong')]",
    viewApiDiv: "//div[@class='key']",

    editApiButton: "//button//span//mat-icon[text()='edit']",
    updateApiButton: "//div[@class='btn-submit']//button//span",

    deleteApiButton: "//button//span//mat-icon[text()='delete']",
    confirmDeleteApiButton: "//button//span[text()=' Đồng ý ']"
}