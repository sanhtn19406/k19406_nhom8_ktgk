const { I } = inject()
const apiKeyFunction = require('../page/apiKey/index');
const Myfunctions = require('../page/common/functions')
const MyVariable = require('../page/common/variable')


Given('I create a API key', () => {
    apiKeyFunction.createApiKey(MyVariable.apiName)
});

Given('I edit API key to {string}', (name) => {
    apiKeyFunction.editApiKey(name)
});

Given('I delete API key', () => {
    apiKeyFunction.deleteApiKey()
});